# 工程简介

GeekAdmin 微服务框架学习交流使用



采用Alibaba 全套技术栈，nacos，sentinel，seata

封装了项目开发过程中常用的场景启动器



[geek-auth-starter](https://gitee.com/geeksuper/geek-admin/tree/master/geek-framework/geek-auth-starter)

[geek-cloud-starter](https://gitee.com/geeksuper/geek-admin/tree/master/geek-framework/geek-cloud-starter)

[geek-common](https://gitee.com/geeksuper/geek-admin/tree/master/geek-framework/geek-common)

[geek-globalexception-starter](https://gitee.com/geeksuper/geek-admin/tree/master/geek-framework/geek-globalexception-starter)

[geek-redis-starter](https://gitee.com/geeksuper/geek-admin/tree/master/geek-framework/geek-redis-starter)

[geek-service-dependencies](https://gitee.com/geeksuper/geek-admin/tree/master/geek-framework/geek-service-dependencies)

[add geek-auth-starter](https://gitee.com/geeksuper/geek-admin/commit/d3f53ac5e98ca6f725202a722a33c8622956ac16)

[geek-swagger-starter](https://gitee.com/geeksuper/geek-admin/tree/master/geek-framework/geek-swagger-starter)

[geek-test-starter](https://gitee.com/geeksuper/geek-admin/tree/master/geek-framework/geek-test-starter)

[geek-verification-code-starter](https://gitee.com/geeksuper/geek-admin/tree/master/geek-framework/geek-verification-code-starter)

