package com.geek.project.geekglobalexceptionstarter.config;

import com.geek.project.geekglobalexceptionstarter.error.GlobalExceptionTranslator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@Configuration
public class GlobalExceptionAutoConfiguration {

    @Bean("globalExceptionTranslator")
    @ConditionalOnMissingBean(name = "globalExceptionTranslator")
    public GlobalExceptionTranslator globalExceptionTranslator(){
        return new GlobalExceptionTranslator();
    }
}
