package com.geek.project.geekcommon.page;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.geek.project.geekcommon.covert.annotation.Default;
import java.io.Serializable;
import java.util.List;

public class PageWrapper<T> implements Serializable {

    private List<?> list;
    private Integer total = 0;
    private Integer pageSize = 0;
    private Integer currPage = 0;
    private Integer totalPage = 0;

    @Default
    public PageWrapper() {
    }

    /**
     * 分页
     * @param list        列表数据
     * @param totalCount  总记录数
     * @param pageSize    每页记录数
     * @param currPage    当前页数
     */
    public PageWrapper(List<T> list, Integer totalCount, Integer pageSize, Integer currPage) {
        this.list = list;
        this.total = totalCount;
        this.pageSize = pageSize;
        this.currPage = currPage;
        this.totalPage = (int)Math.ceil((double)totalCount/pageSize);
    }


    public PageWrapper(IPage<T> page) {
        this.list = page.getRecords();
        this.total= Math.toIntExact(page.getTotal());
        this.pageSize = Math.toIntExact(page.getSize());
        this.currPage = Math.toIntExact(page.getCurrent());
        this.totalPage = Math.toIntExact(page.getPages());
    }

    public PageWrapper(Integer currPage, Integer pageSize) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<T> page = new com.baomidou.mybatisplus.extension.plugins.pagination.Page(currPage, pageSize);
        this.list = page.getRecords();
        this.total= Math.toIntExact(page.getTotal());
        this.pageSize = Math.toIntExact(page.getSize());
        this.currPage = Math.toIntExact(page.getCurrent());
        this.totalPage = Math.toIntExact(page.getPages());
    }


    public List<?> getList() {
        return list;
    }

    public void setList(List<?> list) {
        this.list = list;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getCurrPage() {
        return currPage;
    }

    public void setCurrPage(Integer currPage) {
        this.currPage = currPage;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }
}
