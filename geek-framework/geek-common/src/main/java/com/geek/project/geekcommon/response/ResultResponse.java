package com.geek.project.geekcommon.response;

import com.fasterxml.jackson.core.type.TypeReference;
import com.geek.project.geekcommon.utils.JsonUtils;

import java.util.HashMap;

public class ResultResponse extends HashMap<String, Object> {

        public ResultResponse(){
            put("code", 0);
            put("msg", "success");
        }

        @Override
        public ResultResponse put(String key, Object value) {
            super.put(key, value);
            return this;
        }

        public Integer getCode() {
            return (Integer) this.get("code");
        }

        public <T> T getData(String key, TypeReference<T> type) {
            Object data = get(key);
            String s = JsonUtils.objectToJson(data);
            T t = JsonUtils.jsonToObject(s, type);
            return t;
        }
    }