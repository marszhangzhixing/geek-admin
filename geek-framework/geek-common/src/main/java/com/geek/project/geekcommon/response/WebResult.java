package com.geek.project.geekcommon.response;

public class WebResult {

    private ResultResponse result = new ResultResponse();

    public static WebResult INSTANCE(){
        return new WebResult();
    }

    public WebResult message(String message) {
        result.put("msg", message);
        return this;
    }

    public WebResult code(String code) {
        result.put("code", ResultCode.valueOf(code));
        return this;
    }

    public WebResult success() {
        result.put("msg", ResultCode.SUCCESS.getMsg());
        result.put("code", ResultCode.SUCCESS.getCode());
        result.put("data", null);
        return this;
    }

    public WebResult success(Object data) {
        result.put("msg", ResultCode.SUCCESS.getMsg());
        result.put("code", ResultCode.SUCCESS.getCode());
        result.put("data", data);
        return this;
    }

    public WebResult success(String code, Object data) {
        result.put("msg", ResultCode.SUCCESS.getMsg());
        result.put("code", ResultCode.valueOf(code));
        result.put("data", data);
        return this;
    }

    public WebResult success(String code, String message, Object data) {
        result.put("msg", message);
        result.put("code", ResultCode.valueOf(code));
        result.put("data", data);
        return this;
    }

    public WebResult success(ResultCode resultCode, Object data) {
        result.put("msg", resultCode.getMsg());
        result.put("code", resultCode.getCode());
        result.put("data", data);
        return this;
    }


    public WebResult error(Object data) {
        result.put("msg", ResultCode.FAILURE.getMsg());
        result.put("code", ResultCode.FAILURE.getCode());
        result.put("data", data);
        return this;
    }

    public WebResult error(String code, Object data) {
        result.put("msg", ResultCode.FAILURE.getMsg());
        result.put("code", ResultCode.valueOf(code));
        result.put("data", data);
        return this;
    }

    public WebResult error(String code, String message, Object data) {
        result.put("msg", message);
        result.put("code", ResultCode.valueOf(code));
        result.put("data", data);
        return this;
    }

    public WebResult error(ResultCode resultCode, Object data) {
        result.put("msg", resultCode.getMsg());
        result.put("code", resultCode.getCode());
        result.put("data", data);
        return this;
    }


    public ResultResponse build() {
        return this.result;
    }

}
