package com.geek.project.geekcommon.covert.basic;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.geek.project.geekcommon.page.PageWrapper;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

/**
 * 基础转换类，提供基本的几个方法，直接继承就可以，如果有需要写Mappings的写在 {@link #to(Object)} 方法上
 * 并且接口类上一定要加上 {@link org.mapstruct.Mapper} 注解
 * 默认注解，需要单独定义 如 UserCovertExtends INSTANCE = Mappers.getMapper(UserCovertExtends.class); 以此进行实例创建和调用
 * 或者如下
 *
 * @Mapper(componentModel = "spring") 此注解可通过spring进行注入。
 */
public interface BasicConvertObject<SOURCE, TARGET> {

    @InheritConfiguration
    TARGET toConvertVO(SOURCE source);

    @InheritConfiguration
    List<TARGET> toConvertVOList(List<SOURCE> source);

    @Mappings({
        @Mapping(target = "pageSize", source = "size"),
        @Mapping(target = "currPage", source = "current"),
        @Mapping(target = "list", source = "records"),
        @Mapping(target = "totalPage", source = "pages")
    })
    @InheritConfiguration
    PageWrapper<TARGET> toConvertVOPage(Page<SOURCE> source);

    @InheritInverseConfiguration
    SOURCE fromConvertEntity(TARGET source);

    @InheritInverseConfiguration
    List<SOURCE> fromConvertEntityList(List<TARGET> source);
}

