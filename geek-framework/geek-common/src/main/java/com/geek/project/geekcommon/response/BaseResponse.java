package com.geek.project.geekcommon.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BaseResponse {

    public BaseResponse() {
    }

    public BaseResponse(String message, ResultCode code) {
        this.message = message;
        this.code = code;
    }

    private String message;
    @Builder.Default
    private ResultCode code = ResultCode.SUCCESS;

    public boolean isSuccess() {
        return code == ResultCode.SUCCESS;
    }
}
