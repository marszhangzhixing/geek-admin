package com.geek.project.geekredisstarter.service;

import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.ZSetOperations;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * redis 常用方法 公共接口
 *
 * @author geek
 */
public interface RedisService {

    /**
     * 指定缓存失效时间
     *
     * @param key  键
     * @param time 时间(秒)
     * @return
     */
    boolean expire(String key, long time);

    /**
     * 根据key 获取过期时间
     *
     * @param key 键 不能为null
     * @return 时间(秒) 返回0代表为永久有效
     */
    Long getExpire(String key);

    /**
     * 判断key是否存在
     *
     * @param key 键
     * @return true 存在  false不存在
     */
    boolean hasKey(String key);

    /**
     * 删除缓存
     *
     * @param key 可以传一个值 或多个
     * @return
     */
    Boolean del(String... key);

    void batchDelete(String pattern);

    /**
     * 普通缓存获取
     *
     * @param key 键
     * @return 值
     */
    Object get(String key);

    /**
     * 普通缓存放入
     *
     * @param key   键
     * @param value 值
     * @return true成功 false失败
     */
    boolean set(String key, Object value);

    /**
     * 普通缓存放入并设置时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒) time要大于0 如果time小于等于0 将设置无限期
     * @return true成功 false 失败
     */
    boolean set(String key, Object value, long time);

    /**
     * 递增
     *
     * @param key 键
     * @param delta  要增加几(大于0)
     * @return
     */
    Long increment(String key, long delta);

    /**
     * 递增
     *
     * @param key 键
     * @param delta  要增加几(大于0)
     * @return
     */
    Long increment(String key, long delta,Long expire);

    /**
     * 递减
     *
     * @param key 键
     * @param delta  要减少几(小于0)
     * @return
     */
    Long decrease(String key, long delta);

    /**
     * HashGet
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return 值
     */
    Object hashGet(String key, String item);

    /**
     * 获取变量中的键值对
     *
     * @param key 键
     * @return 对应的多个键值
     */
    Map<Object, Object> hashGet(String key);

    /**
     * HashSet 以map集合的形式添加键值对
     *
     * @param key 键
     * @param map 对应多个键值
     * @return true 成功 false 失败
     */
    boolean hashSet(String key, Map<String, Object> map);

    /**
     * HashSet 以map集合的形式添加键值对 并设置时间
     *
     * @param key  键
     * @param map  对应多个键值
     * @param time 时间(秒)
     * @return true成功 false失败
     */
    boolean hashSet(String key, Map<String, Object> map, long time);

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @return true 成功 false失败
     */
    boolean hashSet(String key, String item, Object value);

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @param time  时间(秒) 注意:如果已存在的hash表有时间,这里将会替换原有的时间
     * @return true 成功 false失败
     */
    boolean hashSet(String key, String item, Object value, long time);

    /**
     * 删除hash表中的值
     *
     * @param key  键 不能为null
     * @param item 项 可以使多个 不能为null
     * @return
     */
    Long hashDel(String key, Object... item);

    /**
     * 判断hash表中是否有该项的值
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return true 存在 false不存在
     */
    Boolean hashHasKey(String key, String item);

    /**
     * hash递增 如果不存在,就会创建一个 并把新增后的值返回
     * 变量中的键以double值的大小进行自增长
     * @param key  键
     * @param item 项
     * @param by   要增加几(大于0)
     * @return
     */
    Double hashIncrement(String key, String item, double by);

    /**
     * hash递减
     *
     * @param key  键
     * @param item 项
     * @param by   要减少记(小于0)
     * @return
     */
    Double hashDecrease(String key, String item, double by);

    /**
     * 获取指定变量中的hashMap值
     *
     * @param key  键
     * @return List<Object>
     */
    List<Object> hashValues(String key);

    /**
     * 获取指定变量中的hashMap的键
     *
     * @param key  键
     * @return Set<Object>
     */
    Set<Object> hashKeys(String key);

    // ============================set=============================

    /**
     * 根据key获取Set中的所有值
     *
     * @param key 键
     * @return Set<Object>
     */
    Set<Object> setGet(String key);

    /**
     * 根据value从一个set中查询,是否存在
     *
     * @param key   键
     * @param value 值
     * @return true 存在 false不存在
     */
    Boolean setHasKey(String key, Object value);

    /**
     * 将数据放入set缓存
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 成功个数
     */
    Long setSet(String key, Object... values);

    /**
     * 将set数据放入缓存
     *
     * @param key    键
     * @param time   时间(秒)
     * @param values 值 可以是多个
     * @return 成功个数
     */
    Long setSet(long time,String key, Object... values);

    /**
     * 获取set变量中值的长度
     *
     * @param key 键
     * @return
     */
    Long setSize(String key);

    /**
     * 移除值为value的元素
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 移除的个数
     */
    Long setRemove(String key, Object... values);

    // ===============================list=================================

    /**
     * 获取list缓存的内容
     *
     * @param key   键
     * @param start 开始
     * @param end   结束 0 到 -1代表所有值
     * @return
     */
    List<Object> listGet(String key, long start, long end);

    /**
     * 获取list缓存的长度
     *
     * @param key 键
     * @return
     */
    long listSize(String key);

    /**
     * 通过索引 获取list中的值
     *
     * @param key   键
     * @param index 索引 index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
     * @return
     */
    Object listGet(String key, long index);

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    Long listSet(String key, Object value);

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    Long listSet(String key, Object value, long time);

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    Long listSet(String key, List<Object> value);

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    Long listSet(String key, List<Object> value, long time);

    /**
     * 在集合的指定位置插入元素,如果指定位置已有元素，则覆盖，没有则新增，超过集合下标+n则会报错
     * @param key   键
     * @param index 索引
     * @param value 值
     * @return
     */
    boolean listSet(String key, long index, Object value);

    /**
     * 移除N个值为value
     *
     * @param key   键
     * @param count 移除多少个
     * @param value 值
     * @return 移除的个数
     */
    long lRemove(String key, long count, Object value);

    // ===============================sorted set=================================

    /**
     * 向有序集合添加一个成员的
     *
     * ZADD key score1 member1 [score2 member2]
     *
     */
    boolean zadd(String key, Object member, double score, long time);

    /**
     *     ZRANGEBYSCORE key min max [WITHSCORES] [LIMIT]
     通过分数返回有序集合指定区间内的成员
     *
     */
    Set<Object> zRangeByScore(String key, double minScore, double maxScore);

    /**
     *  ZSCORE key member
     *  返回有序集中，成员的分数值
     *
     */
    Double zscore(String key, Object member);

    /**
     *     ZRANK key member 返回有序集合中指定成员的索引
     *
     */
    Long zrank(String key, Object member);

    /**
     * Zscan 迭代有序集合中的元素（包括元素成员和元素分值）
     *
     */
    Cursor<ZSetOperations.TypedTuple<Object>> zscan(String key);

}
