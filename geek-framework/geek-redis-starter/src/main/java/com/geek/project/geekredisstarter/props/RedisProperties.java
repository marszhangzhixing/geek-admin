package com.geek.project.geekredisstarter.props;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * RedisProperties 配置
 *
 * @author geek
 */
@ConfigurationProperties(prefix = "geek.redis")
public class RedisProperties {

    private String redisPrefix = "geek:project";

    public String getRedisPrefix() {
        return redisPrefix;
    }

    public void setRedisPrefix(String redisPrefix) {
        this.redisPrefix = redisPrefix;
    }
}
