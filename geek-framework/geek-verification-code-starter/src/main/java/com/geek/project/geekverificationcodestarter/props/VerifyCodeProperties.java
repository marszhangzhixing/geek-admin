package com.geek.project.geekverificationcodestarter.props;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * VerifyCodeProperties 配置
 *
 * @author geek
 */
@ConfigurationProperties(prefix = "geek.verify")
public class VerifyCodeProperties {

    /**
     *过期时间
     */
    private long expireTime = 2*60*60L;
    /**
     * 短信验证码前缀
     */
    private String phonePrefix = "sms";
    /**
     * 短信每日发送上限
     * 注: 最大上限值，以短信平台上限值为准
     */
    private int maxLimit = 5;

    public long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(long expireTime) {
        this.expireTime = expireTime;
    }

    public String getPhonePrefix() {
        return phonePrefix;
    }

    public void setPhonePrefix(String phonePrefix) {
        this.phonePrefix = phonePrefix;
    }

    public int getMaxLimit() {
        return maxLimit;
    }

    public void setMaxLimit(int maxLimit) {
        this.maxLimit = maxLimit;
    }
}
