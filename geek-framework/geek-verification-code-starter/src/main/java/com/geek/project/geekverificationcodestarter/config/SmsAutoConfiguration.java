package com.geek.project.geekverificationcodestarter.config;

import com.aliyuncs.IAcsClient;
import com.geek.project.geekverificationcodestarter.props.SmsProperties;
import com.geek.project.geekverificationcodestarter.service.sms.SmsService;
import com.geek.project.geekverificationcodestarter.service.sms.impl.AliYunSmsServiceImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 短信服务 自动装配
 *
 * @author geek
 */
@Configuration
@ConditionalOnClass(value = {IAcsClient.class})
@EnableConfigurationProperties(SmsProperties.class)
public class SmsAutoConfiguration {

    @Bean(name = "smsService")
    @ConditionalOnMissingBean(name = "smsService")
    public SmsService smsService(SmsProperties smsProperties){
        return new AliYunSmsServiceImpl(smsProperties);
    }

}
