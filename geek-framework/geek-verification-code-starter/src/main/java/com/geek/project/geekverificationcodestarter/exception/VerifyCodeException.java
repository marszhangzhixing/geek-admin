package com.geek.project.geekverificationcodestarter.exception;

public class VerifyCodeException extends RuntimeException {

    private String message;
    private int code;

    public VerifyCodeException(String message, Throwable cause) {
        super(message, cause);
    }

    public VerifyCodeException(String message, int code) {
        super(message);
        this.message = message;
        this.code = code;
    }

    public VerifyCodeException(String message) {
        super(message);
    }
}
