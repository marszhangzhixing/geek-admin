package com.geek.project.geekverificationcodestarter.utils;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class DateUtil {

    /**
     * 获取当日剩余时间 秒 long值
     *
     * @return long
     */
    public static Long getTimeLeftSeconds() {
        LocalDateTime midnight = LocalDateTime.now().plusDays(1).withHour(0).withMinute(0).withSecond(0).withNano(0);
        long seconds = ChronoUnit.SECONDS.between(LocalDateTime.now(), midnight);
        return seconds;
    }

    /**
     * 获取当日剩余时间 毫秒 long值
     *
     * @return long
     */
    public static Long getTimeLeftMillSeconds() {
        LocalDateTime midnight = LocalDateTime.now().plusDays(1).withHour(0).withMinute(0).withSecond(0).withNano(0);
        long millSeconds = ChronoUnit.MILLIS.between(LocalDateTime.now(),midnight);
        return millSeconds;
    }
}
