package com.geek.project.geekverificationcodestarter.service.kaptcha;

import java.util.Map;

/**
 * 验证码 公共接口
 *
 * @author geek
 */
public interface VerifyCodeService {

    /**
     * 生成验证码
     *
     * @author geek
     * @param key
     * @return
     */
    Map<String, String> generalVerifyCode(String key);

    /**
     * 验证验证码
     *
     * @author geek
     * @param key
     * @param code
     * @return
     */
    boolean verifyCode(String key,String code);

    /**
     * 发送手机验证码
     *
     * @author geek
     * @param phone
     * @return
     */
    Map<String, String> generalPhoneVerifyCode(String phone);

    String sendLogin(String phone, String phoneCode);
    /**
     * 验证手机验证码
     *
     * @author geek
     * @param phone
     * @param code
     * @return
     */
    boolean verifyPhoneCode(String phone,String code);

}
