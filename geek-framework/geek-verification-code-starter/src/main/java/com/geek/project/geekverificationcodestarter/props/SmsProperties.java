package com.geek.project.geekverificationcodestarter.props;

import com.aliyuncs.profile.DefaultProfile;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;


/**
 * SmsProperties 配置
 *
 * @author geek
 */
@ConfigurationProperties(prefix = "geek.sms")
public class SmsProperties {

    private String accessKeyId;

    private String accessSecret;

    private String signName;

    private boolean enabled;

    private Map templateCode;


    private DefaultProfile profile;

    public DefaultProfile getProfile() {
        return DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessSecret);
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getAccessSecret() {
        return accessSecret;
    }

    public void setAccessSecret(String accessSecret) {
        this.accessSecret = accessSecret;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Map<String, String> getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(Map templateCode) {
        this.templateCode = templateCode;
    }
}
