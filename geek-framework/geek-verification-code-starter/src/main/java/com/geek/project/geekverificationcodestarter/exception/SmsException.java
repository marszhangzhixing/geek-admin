package com.geek.project.geekverificationcodestarter.exception;

public class SmsException extends RuntimeException{

    private String message;
    private int code;

    public SmsException(String message, Throwable cause) {
        super(message, cause);
    }

    public SmsException(String message, int code) {
        super(message);
        this.message = message;
        this.code = code;
    }

    public SmsException(String message) {
        super(message);
    }
}
