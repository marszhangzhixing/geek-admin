package com.geek.project.geekverificationcodestarter.service.sms;

/**
 * 短信发送 公共接口
 *
 * @author geek
 */
public interface SmsService {

    /**
     * 发送短信模板信息
     *
     * @param phone 电话号码
     * @param templateCode 验证码
     * @param templateContent 模板数据
     * @return string
     */
    String sendSms(String phone, String templateCode, String templateContent);

}
