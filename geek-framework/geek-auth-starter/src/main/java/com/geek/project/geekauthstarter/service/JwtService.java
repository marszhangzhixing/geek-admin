package com.geek.project.geekauthstarter.service;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;

import java.util.Map;

/**
 * JwtService 公共接口
 *
 * @author geek
 */
public interface JwtService {

    /**
     * 私钥加密token
     *
     * @param map           载荷中的数据
     * @param expireMinutes 过期时间，单位秒
     * @return
     * @throws Exception
     */
    String generateToken(Map<String, Object> map, int expireMinutes);

    /**
     * 私钥加密token
     *
     * @param map           载荷中的数据
     * @return
     * @throws Exception
     */
    String generateToken(Map<String, Object> map);

    /**
     * 公钥解析token
     *
     * @param token  用户请求中的token
     * @return
     * @throws Exception
     */
    Jws<Claims> parserToken(String token);

    /**
     * 获取token中的用户信息
     *
     * @param token  用户请求中的令牌
     * @return 用户信息
     * @throws Exception
     */
    Map<String, Object> getInfoFromToken(String token);
}
