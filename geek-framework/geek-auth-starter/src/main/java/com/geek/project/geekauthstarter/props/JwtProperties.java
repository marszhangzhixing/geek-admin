package com.geek.project.geekauthstarter.props;

import cn.hutool.core.util.StrUtil;
import com.geek.project.geekauthstarter.utils.RsaUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;
import java.io.File;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * JwtProperties 配置
 *
 * @author geek
 */
@Slf4j
@ConfigurationProperties(prefix = "geek.auth.jwt")
public class JwtProperties {

    private String pubKeyPath;
    private String priKeyPath;
    private String secret;
    private String cookieName;
    private Integer expire;
    private String unick;
    private PublicKey publicKey;
    private PrivateKey privateKey;

    @PostConstruct
    public void init(){
        try{
            File pubFile = new File(this.pubKeyPath);
            if (!pubFile.exists()) {
                throw new IllegalArgumentException("公钥地址错误，请输入绝对路径");
            }
            if(StrUtil.isNotBlank(this.priKeyPath)){
                this.privateKey = RsaUtils.getPrivateKey(this.priKeyPath);
            }
            this.publicKey = RsaUtils.getPublicKey(this.pubKeyPath);
        } catch (Exception e){
            log.error("密钥初始化异常", e);
        }

    }

    public String getPubKeyPath() {
        return pubKeyPath;
    }

    public void setPubKeyPath(String pubKeyPath) {
        this.pubKeyPath = pubKeyPath;
    }

    public String getPriKeyPath() {
        return priKeyPath;
    }

    public void setPriKeyPath(String priKeyPath) {
        this.priKeyPath = priKeyPath;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getCookieName() {
        return cookieName;
    }

    public void setCookieName(String cookieName) {
        this.cookieName = cookieName;
    }

    public Integer getExpire() {
        return expire;
    }

    public void setExpire(Integer expire) {
        this.expire = expire;
    }

    public String getUnick() {
        return unick;
    }

    public void setUnick(String unick) {
        this.unick = unick;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }
}
