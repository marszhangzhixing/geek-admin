package com.geek.project.geekauthstarter.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import java.util.Map;

/**
 * Jackson 工具类 {@link JsonUtils}
 *
 * @author Kevin
 * @email: 178676392@qq.com
 */
public class JsonUtils {

    /**
     * 定义jackson对象
     */
    private static final ObjectMapper MAPPER = new ObjectMapper();

    /**
     * 将对象转换成json字符串
     *
     * @param data
     * @return
     */
    public static String objectToJson(Object data) {
        try {
            String string = MAPPER.writeValueAsString(data);
            return string;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将json结果集转化为对象
     *
     * @param jsonData json数据
     * @param beanType 对象类型
     * @return
     */
    public static <T> T jsonToPojo(String jsonData, Class<T> beanType) {
        try {
            T t = MAPPER.readValue(jsonData, beanType);
            return t;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T> T jsonToObject(String jsonData, TypeReference<T> typeReference) {
        try {
            T t = MAPPER.readValue(jsonData, typeReference);
            return t;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将json数据转换成List
     *
     * @param jsonData
     * @param beanType
     * @return
     */
    public static <T> List<T> jsonToList(String jsonData, Class<T> beanType) {
        JavaType javaType = MAPPER.getTypeFactory().constructParametricType(List.class, beanType);
        try {
            List<T> list = MAPPER.readValue(jsonData, javaType);
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将json数据转换为Map<String, List<T>>
     * @param jsonData
     * @param beanType
     * @param <T>
     * @return
     */
    public static <T> Map<String, List<T>> jsonToMapList(String jsonData, Class<T> beanType) {
        try {
            Map<String, List<T>> stringListMap = MAPPER.readValue(
                    jsonData, new TypeReference<Map<String, List<T>>>() {});
            return stringListMap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        String json = "{\"id\":7559333981,\"idstr\":\"7559333981\",\"class\":1,\"screen_name\":\"Kevin-3088\",\"name\":\"Kevin-3088\",\"province\":\"44\",\"city\":\"1000\",\"location\":\"广东\",\"description\":\"\",\"url\":\"\",\"profile_image_url\":\"https://tvax1.sinaimg.cn/default/images/default_avatar_male_50.gif?KID=imgbed,tva&Expires=1612447318&ssig=%2Bbny5%2FoLRs\",\"profile_url\":\"u/7559333981\",\"domain\":\"\",\"weihao\":\"\",\"gender\":\"m\",\"followers_count\":1,\"friends_count\":59,\"pagefriends_count\":0,\"statuses_count\":1,\"video_status_count\":0,\"video_play_count\":0,\"favourites_count\":0,\"created_at\":\"Wed Feb 03 17:41:40 +0800 2021\",\"following\":false,\"allow_all_act_msg\":false,\"geo_enabled\":true,\"verified\":false,\"verified_type\":-1,\"remark\":\"\",\"insecurity\":{\"sexual_content\":false},\"status\":{\"visible\":{\"type\":0,\"list_id\":0},\"created_at\":\"Thu Feb 04 18:56:53 +0800 2021\",\"id\":4600953308722052,\"idstr\":\"4600953308722052\",\"mid\":\"4600953308722052\",\"can_edit\":false,\"show_additional_indication\":0,\"text\":\"#集福牛开福运# 开年惊喜~我收到了 @让红包飞 送我的#平安就牛#福牛卡，离除夕分1亿的小目标又进了一步！[太开心]集齐6种福牛卡，除夕分1亿，最高能赢2021元！收集新年好福运吧！#让红包飞# 快戳>> http://t.cn/A65UMIBm\",\"textLength\":204,\"source_allowclick\":0,\"source_type\":1,\"source\":\"<a href=\\\"http://app.weibo.com/t/feed/LdSIi\\\" rel=\\\"nofollow\\\">红包活动</a>\",\"favorited\":false,\"truncated\":false,\"in_reply_to_status_id\":\"\",\"in_reply_to_user_id\":\"\",\"in_reply_to_screen_name\":\"\",\"pic_urls\":[],\"geo\":null,\"is_paid\":false,\"mblog_vip_type\":0,\"reposts_count\":0,\"comments_count\":0,\"attitudes_count\":0,\"pending_approval_count\":0,\"isLongText\":false,\"reward_exhibition_type\":0,\"hide_flag\":0,\"mlevel\":0,\"biz_ids\":[231966],\"biz_feature\":0,\"page_type\":64,\"hasActionTypeCard\":0,\"darwin_tags\":[],\"hot_weibo_tags\":[],\"text_tag_tips\":[],\"mblogtype\":0,\"rid\":\"0\",\"userType\":0,\"more_info_type\":0,\"positive_recom_flag\":0,\"content_auth\":0,\"gif_ids\":\"\",\"is_show_bulletin\":2,\"comment_manage_info\":{\"comment_permission_type\":-1,\"approval_comment_type\":0},\"pic_num\":0},\"ptype\":0,\"allow_all_comment\":true,\"avatar_large\":\"https://tvax1.sinaimg.cn/default/images/default_avatar_male_180.gif?KID=imgbed,tva&Expires=1612447318&ssig=jkOaT55DIZ\",\"avatar_hd\":\"https://tvax1.sinaimg.cn/default/images/default_avatar_male_180.gif?KID=imgbed,tva&Expires=1612447318&ssig=jkOaT55DIZ\",\"verified_reason\":\"\",\"verified_trade\":\"\",\"verified_reason_url\":\"\",\"verified_source\":\"\",\"verified_source_url\":\"\",\"follow_me\":false,\"like\":false,\"like_me\":false,\"online_status\":0,\"bi_followers_count\":0,\"lang\":\"zh-cn\",\"star\":0,\"mbtype\":0,\"mbrank\":0,\"block_word\":0,\"block_app\":0,\"credit_score\":80,\"user_ability\":0,\"urank\":0,\"story_read_state\":-1,\"vclub_member\":0,\"is_teenager\":0,\"is_guardian\":0,\"is_teenager_list\":0,\"pc_new\":0,\"special_follow\":false,\"planet_video\":0,\"video_mark\":0,\"live_status\":0,\"user_ability_extend\":0}";

        Map<String, String> map = JsonUtils.jsonToObject(json, new TypeReference<Map>() {
        });

        System.out.println(map.get("name"));
    }
}
