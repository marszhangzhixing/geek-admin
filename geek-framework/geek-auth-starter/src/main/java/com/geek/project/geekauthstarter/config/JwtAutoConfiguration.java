package com.geek.project.geekauthstarter.config;

import com.geek.project.geekauthstarter.props.JwtProperties;
import com.geek.project.geekauthstarter.service.JwtService;
import com.geek.project.geekauthstarter.service.impl.JwtServiceImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * JwtService 自动装配
 *
 * @author geek
 */
@Configuration
@EnableConfigurationProperties(JwtProperties.class)
public class JwtAutoConfiguration {

    /**
     * @author 
     * @date 2021/5/27
     * @param jwtProperties 
     * @return com.geek.project.geekauthstarter.service.JwtService
     */
    @Bean("jwtService")
    @ConditionalOnMissingBean(name = "jwtService")
    public JwtService jwtService(JwtProperties jwtProperties){
        return new JwtServiceImpl(jwtProperties);
    }
}
