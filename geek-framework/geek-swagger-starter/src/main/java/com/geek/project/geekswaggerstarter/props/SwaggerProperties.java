package com.geek.project.geekswaggerstarter.props;

import com.geek.project.geekswaggerstarter.model.MyContact;
import com.geek.project.geekswaggerstarter.model.MyGroup;
import org.springframework.boot.context.properties.ConfigurationProperties;
import springfox.documentation.service.VendorExtension;

import java.util.ArrayList;
import java.util.List;

/**
 * SwaggerProperties 配置
 *
 * @author geek
 */
@ConfigurationProperties(prefix = "geek.swagger")
public class SwaggerProperties {

    public static final MyContact DEFAULT_CONTACT = new MyContact();

    /**
     * 文档title
     */
    private String title;

    /**
     * 文档描述
     */
    private String description;

    /**
     * 版本
     */
    private String version = "v3.0";

    /**
     * 指定host
     */
    private String host = "";

    /**
     * 组织链接
     */
    private String termsOfServiceUrl = "";

    /**
     * 联系人信息
     */
    private MyContact contact = DEFAULT_CONTACT;

    /**
     * 许可
     */
    private String license = "";

    /**
     * 许可链接
     */
    private  String licenseUrl = "";

    /**
     *
     */
    private List<VendorExtension> vendorExtensions = new ArrayList();

    /**
     * 扫描包
     */
    private List<MyGroup> groups = new ArrayList<>();

    /**
     * 令牌参数名称
     */
    private String tokenName = "Authorization";

    /**
     * 是否开启swagger
     */
    private Boolean enabled = false;

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getTermsOfServiceUrl() {
        return termsOfServiceUrl;
    }

    public void setTermsOfServiceUrl(String termsOfServiceUrl) {
        this.termsOfServiceUrl = termsOfServiceUrl;
    }

    public MyContact getContact() {
        return contact;
    }

    public void setContact(MyContact contact) {
        this.contact = contact;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getLicenseUrl() {
        return licenseUrl;
    }

    public void setLicenseUrl(String licenseUrl) {
        this.licenseUrl = licenseUrl;
    }

    public List<VendorExtension> getVendorExtensions() {
        return vendorExtensions;
    }

    public void setVendorExtensions(List<VendorExtension> vendorExtensions) {
        this.vendorExtensions = vendorExtensions;
    }

    public List<MyGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<MyGroup> groups) {
        this.groups = groups;
    }

    public String getTokenName() {
        return tokenName;
    }

    public void setTokenName(String tokenName) {
        this.tokenName = tokenName;
    }

    public SwaggerProperties() {
    }
}
