package com.geek.project.geekswaggerstarter.model;

/**
 *
 * @author: geek
 */
public class MyGroup {

    /**
     * 分组名称
     */
    private String groupName;

    /**
     * 分组包名
     */
    private String basePackage;

    /**
     * 是否需要携带令牌 false 不需要
     */
    private boolean isLogin = false;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getBasePackage() {
        return basePackage;
    }

    public void setBasePackage(String basePackage) {
        this.basePackage = basePackage;
    }

    public boolean isLogin() {
        return isLogin;
    }

    public void setLogin(boolean login) {
        isLogin = login;
    }
}
