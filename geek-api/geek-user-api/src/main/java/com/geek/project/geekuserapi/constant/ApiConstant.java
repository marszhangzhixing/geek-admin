package com.geek.project.geekuserapi.constant;


public interface ApiConstant {

    /**
     * 服务名称
     */
    String SERVICE_NAME = "geek-user";
    /**
     * 用户分页数据
     */
    String USER_FIND_ACCOUNT = "find";
    /**
     * 用户分页数据
     */
    String USER_PAGE = "page";
    /**
     * 用户列表数据
     */
    String USER_LIST = "list";
    /**
     * 用户详情
     */
    String USER_INFO = "info/{id}";
    /**
     * 添加用户
     */
    String USER_SAVE = "save";
    /**
     * 修改用户
     */
    String USER_UPDATE = "update";
    /**
     * 删除用户
     */
    String USER_DELETE = "delete";
}
