package com.geek.project.geekuserapi.feign;


import com.geek.project.geekcommon.response.BaseResponse;
import com.geek.project.geekuserapi.constant.ApiConstant;
import com.geek.project.geekuserapi.model.UserVo;
import com.geek.project.geekuserapi.model.dto.UserListResponse;
import com.geek.project.geekuserapi.model.dto.UserResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@FeignClient(value = ApiConstant.SERVICE_NAME, path = "/")
public interface UserFeign {

    @PostMapping(ApiConstant.USER_LIST)
    UserListResponse list(@RequestBody UserVo userVo);

    @GetMapping(ApiConstant.USER_INFO)
    UserResponse info(@PathVariable("id") Long id);

    @PostMapping(ApiConstant.USER_SAVE)
    BaseResponse save(@RequestBody UserVo userVo);

    @PostMapping(ApiConstant.USER_UPDATE)
    BaseResponse update(@RequestBody UserVo userVo);

    @PostMapping(ApiConstant.USER_DELETE)
    BaseResponse delete(@RequestBody Long[] ids);

}
