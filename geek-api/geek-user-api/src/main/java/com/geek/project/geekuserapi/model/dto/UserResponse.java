package com.geek.project.geekuserapi.model.dto;

import com.geek.project.geekcommon.response.BaseResponse;
import com.geek.project.geekuserapi.model.UserVo;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserResponse extends BaseResponse {
    UserVo data;
}
