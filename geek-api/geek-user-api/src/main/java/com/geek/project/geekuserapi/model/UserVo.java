package com.geek.project.geekuserapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserVo {

    private Long id;
    private String name1;
    private Integer age;
    private String email;
    private String passwd;

}
