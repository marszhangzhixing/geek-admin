package com.geek.project.geekuserapi.model.dto;

import com.geek.project.geekcommon.response.BaseResponse;
import com.geek.project.geekuserapi.model.UserVo;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class UserListResponse extends BaseResponse {
    List<UserVo> list;
}
