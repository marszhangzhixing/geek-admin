package com.geek.project.geekauthapi.feign;

import com.geek.project.geekauthapi.constant.ApiConstant;
import com.geek.project.geekauthapi.model.vo.LoginVo;
import com.geek.project.geekcommon.response.ResultResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@FeignClient(value = ApiConstant.SERVICE_NAME, path = "/")
public interface AuthFeign {

    @PostMapping(ApiConstant.AUTH_LOGIN)
    ResultResponse login(@RequestBody @Validated LoginVo user, @ApiIgnore HttpServletRequest request, @ApiIgnore HttpServletResponse response);

    @PostMapping(ApiConstant.AUTH_LOGOUT)
    ResultResponse logout(@RequestHeader(name="token") String token);

    @PostMapping(ApiConstant.AUTH_SEND_SMS)
    ResultResponse sendSms(@PathVariable("phone") String Phone);

}
