package com.geek.project.geekauthapi.constant;


public interface ApiConstant {

    /**
     * 服务名称
     */
    String SERVICE_NAME = "geek-auth";
    /**
     * 登录
     */
    String AUTH_LOGIN = "login";
    /**
     * 登出
     */
    String AUTH_LOGOUT = "logout";
    /**
     * 发送短信验证码
     */
    String AUTH_SEND_SMS = "send/{phone}";


}
