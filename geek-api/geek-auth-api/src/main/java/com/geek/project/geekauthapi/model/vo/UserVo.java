package com.geek.project.geekauthapi.model.vo;

//import com.geek.project.geekcommon.covert.annotation.Default;
//import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//import javax.validation.constraints.NotBlank;


//@ApiModel
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserVo {

    private Long id;
    private String name1;
    private Integer age;
    private String email;
    private String passwd;

}
