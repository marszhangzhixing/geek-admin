package com.geek.project.geekauth.exception;


public class AuthException extends RuntimeException {

    private String message;
    private int code;

    public AuthException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthException(String message, int code) {
        super(message);
        this.message = message;
        this.code = code;
    }

    public AuthException(String message) {
        super(message);
    }
}
