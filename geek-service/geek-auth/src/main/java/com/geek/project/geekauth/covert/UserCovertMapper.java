package com.geek.project.geekauth.covert;

import com.geek.project.geekauth.entity.UserEntity;
import com.geek.project.geekauthapi.model.vo.UserVo;
import com.geek.project.geekcommon.covert.basic.BasicConvertObject;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;


@Mapper(componentModel = "spring")
public interface UserCovertMapper extends BasicConvertObject<UserEntity, UserVo> {
    UserCovertMapper INSTANCE = Mappers.getMapper(UserCovertMapper.class);

    @Mappings({
        @Mapping(target = "name1", source = "name")
    })
    @Override
    UserVo toConvertVO(UserEntity userEntity);

    @Mappings({
        @Mapping(target = "name", source = "name1")
    })
    @Override
    UserEntity fromConvertEntity(UserVo userVo);
}
