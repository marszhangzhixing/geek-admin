package com.geek.project.geekauth.controller.login;

import com.geek.project.geekauth.service.impl.LoginServiceImpl;
import com.geek.project.geekauthapi.feign.AuthFeign;
import com.geek.project.geekauthapi.model.vo.LoginVo;
import com.geek.project.geekcommon.response.ResultResponse;
import com.geek.project.geekcommon.response.WebResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ResponseHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Api(value = "LoginController", tags = "LoginController")
@RestController
public class LoginController implements AuthFeign {

    @Autowired
    private LoginServiceImpl loginService;

    @ApiOperation(value = "登录")
    @ResponseHeader(name = "authorization")
    @Override
    public ResultResponse login(@RequestBody @Validated LoginVo user, @ApiIgnore HttpServletRequest request, @ApiIgnore HttpServletResponse response){
        loginService.login(user, request, response);
        return WebResult.INSTANCE().success(user).build();
    }

    @ApiOperation(value = "登出")
    @Override
    public ResultResponse logout(@RequestHeader(name="token") String token) {
        loginService.logout(token);
        return WebResult.INSTANCE().success().build();
    }

    @ApiOperation(value = "发送短信验证码")
    @Override
    public ResultResponse sendSms(String phone) {
        loginService.sendSms(phone);
        return WebResult.INSTANCE().success().build();
    }

}
