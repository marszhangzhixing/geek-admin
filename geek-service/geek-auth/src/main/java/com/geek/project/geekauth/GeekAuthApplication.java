package com.geek.project.geekauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients(basePackages = {"com.geek.project.geekauthapi.feign"})
@EnableDiscoveryClient
@SpringBootApplication
public class GeekAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(GeekAuthApplication.class, args);
    }

}
