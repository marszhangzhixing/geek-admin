package com.geek.project.geekauth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.geek.project.geekauth.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<UserEntity>{

}