package com.geek.project.geekauth.controller.login;

import com.geek.project.geekauthstarter.props.JwtProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "AuthController")
@RestController
@RequestMapping("/admin/auth")
public class AuthController {

    @Autowired
    JwtProperties jwtProperties;
    @Autowired
    ApplicationContext applicationContext;

    @ApiOperation(value = "auth")
    @GetMapping("auth")
    public String[] auth(){
        return applicationContext.getBeanDefinitionNames();
    }

}
