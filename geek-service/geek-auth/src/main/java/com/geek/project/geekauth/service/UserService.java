package com.geek.project.geekauth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.geek.project.geekauth.entity.UserEntity;
import com.geek.project.geekauthapi.model.vo.LoginVo;
import com.geek.project.geekauthapi.model.vo.UserVo;
import com.geek.project.geekcommon.page.PageWrapper;

import java.util.List;
import java.util.Map;

public interface UserService extends IService<UserEntity> {

    UserVo findUserByAccount(LoginVo loginVo);

    List<UserVo> queryList(UserVo userVo);

    PageWrapper<UserVo> queryPageList(Map<String, Object> params);

    PageWrapper<UserVo> queryPageList(UserVo userVo, Long page, Long size);
}
