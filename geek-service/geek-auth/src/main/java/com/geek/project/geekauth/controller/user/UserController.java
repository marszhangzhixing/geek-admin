package com.geek.project.geekauth.controller.user;

import com.geek.project.geekauth.covert.UserCovertMapper;
import com.geek.project.geekauth.entity.UserEntity;
import com.geek.project.geekauth.service.UserService;
import com.geek.project.geekauthapi.model.vo.UserVo;
import com.geek.project.geekcommon.page.PageWrapper;
import com.geek.project.geekcommon.response.BaseResponse;
import com.geek.project.geekuserapi.model.dto.UserListResponse;
import com.geek.project.geekuserapi.model.dto.UserPageResponse;
import com.geek.project.geekuserapi.model.dto.UserResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;


@Api(value = "userController", tags = "geek688")
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public UserListResponse list(@RequestBody UserVo userVo){
        List<UserVo> lists = userService.queryList(userVo);
//        return new UserListResponse(lists);
        return null;
    }

    @ApiOperation("/page")
    @PostMapping("/page")
    public UserPageResponse page(@RequestBody UserVo userVo){
        PageWrapper page = userService.queryPageList(userVo, 1L, 10L);
        return new UserPageResponse(page);
    }

    /**
     * 信息
     */
    @ApiOperation("/info/{id}")
    @GetMapping("/info/{id}")
    public UserResponse info(@PathVariable("id") Long id){
        UserEntity u = userService.getById(id);
        UserVo data = UserCovertMapper.INSTANCE.toConvertVO(u);
//        return new UserResponse(data);
        return null;
    }

    /**
     * 保存
     */
    @ApiOperation("save")
    @PostMapping("/save")
    public BaseResponse save(@RequestBody UserVo user){
        UserEntity entity = UserCovertMapper.INSTANCE.fromConvertEntity(user);
        userService.save(entity);
        return BaseResponse.builder().build();
    }

    /**
     * 修改
     */
    @ApiOperation("update")
    @PostMapping("/update")
    public BaseResponse update(@RequestBody UserVo user){
        UserEntity entity = UserCovertMapper.INSTANCE.fromConvertEntity(user);
        userService.updateById(entity);
        return BaseResponse.builder().build();
    }

    /**
     * 删除
     */
    @ApiOperation("delete")
    @PostMapping("/delete")
    public BaseResponse delete(@RequestBody Long[] ids){
        userService.removeByIds(Arrays.asList(ids));
        return BaseResponse.builder().build();
    }
    

}
