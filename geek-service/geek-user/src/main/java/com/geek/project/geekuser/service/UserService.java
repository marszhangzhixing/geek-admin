package com.geek.project.geekuser.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.geek.project.geekcommon.page.PageWrapper;
import com.geek.project.geekuser.entity.UserEntity;
import com.geek.project.geekuserapi.model.UserVo;

import java.util.List;
import java.util.Map;

public interface UserService extends IService<UserEntity> {

    UserVo findUserByAccount(UserVo userVo);

    List<UserVo> queryList(UserVo userVo);

    PageWrapper<UserVo> queryPageList(Map<String, Object> params);

    PageWrapper<UserVo> queryPageList(UserVo userVo, Long page, Long size);
}
