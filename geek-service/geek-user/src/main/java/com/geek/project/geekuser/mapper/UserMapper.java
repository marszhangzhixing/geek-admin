package com.geek.project.geekuser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.geek.project.geekuser.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<UserEntity>{

}