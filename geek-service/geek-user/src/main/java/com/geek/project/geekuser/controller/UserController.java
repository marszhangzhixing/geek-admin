package com.geek.project.geekuser.controller;

import com.geek.project.geekcommon.page.PageWrapper;
import com.geek.project.geekcommon.response.BaseResponse;
import com.geek.project.geekcommon.response.ResultResponse;
import com.geek.project.geekcommon.response.WebResult;
import com.geek.project.geekuser.covert.UserCovertMapper;
import com.geek.project.geekuser.entity.UserEntity;
import com.geek.project.geekuser.service.UserService;
import com.geek.project.geekuserapi.feign.UserFeign;
import com.geek.project.geekuserapi.model.UserVo;
import com.geek.project.geekuserapi.model.dto.UserPageResponse;
import com.geek.project.geekuserapi.model.dto.UserResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;


@Api(value = "userController", tags = "geek688")
@RestController
@RequestMapping("/user")
public class UserController implements UserFeign {

    @Autowired
    UserService userService;

    /**
     * 列表
     */
//    @RequestMapping("/list")
//    public UserListResponse list(@RequestBody UserVo userVo){
//        List<UserVo> lists = userService.queryList(userVo);
//        return new UserListResponse(lists);
//    }

    @ApiOperation("/page")
    @PostMapping("/page")
    public UserPageResponse page(@RequestBody UserVo userVo){
        PageWrapper page = userService.queryPageList(userVo, 1L, 10L);
        return new UserPageResponse(page);
    }

    @Override
    public ResultResponse findUserByAccount(UserVo userVo) {
        UserVo data = userService.findUserByAccount(userVo);
        return WebResult.INSTANCE().success(data).build();
    }

    @Override
    public UserPageResponse list(UserVo userVo) {
        return null;
    }

    /**
     * 信息
     */
    @ApiOperation("/info/{id}")
    @GetMapping("/info/{id}")
    public UserResponse info(@PathVariable("id") Long id){
        UserEntity u = userService.getById(id);
        UserVo data = UserCovertMapper.INSTANCE.toConvertVO(u);
        return new UserResponse(data);
    }

    /**
     * 保存
     */
    @ApiOperation("save")
    @PostMapping("/save")
    public BaseResponse save(@RequestBody UserVo user){
        UserEntity entity = UserCovertMapper.INSTANCE.fromConvertEntity(user);
        userService.save(entity);
        return BaseResponse.builder().build();
    }

    /**
     * 修改
     */
    @ApiOperation("update")
    @PostMapping("/update")
    public BaseResponse update(@RequestBody UserVo user){
        UserEntity entity = UserCovertMapper.INSTANCE.fromConvertEntity(user);
        userService.updateById(entity);
        return BaseResponse.builder().build();
    }

    /**
     * 删除
     */
    @ApiOperation("delete")
    @PostMapping("/delete")
    public BaseResponse delete(@RequestBody Long[] ids){
        userService.removeByIds(Arrays.asList(ids));
        return BaseResponse.builder().build();
    }
    

}
