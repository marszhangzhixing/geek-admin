package com.geek.project.geekuser;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

//@MapperScan(value = {"com.geek.project.geekuser.mapper"})
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class GeekUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(GeekUserApplication.class, args);
    }

}
