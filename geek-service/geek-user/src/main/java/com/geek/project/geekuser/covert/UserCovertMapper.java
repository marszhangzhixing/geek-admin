package com.geek.project.geekuser.covert;

import com.geek.project.geekcommon.covert.basic.BasicConvertObject;
import com.geek.project.geekuser.entity.UserEntity;
import com.geek.project.geekuserapi.model.UserVo;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;


@Mapper(componentModel = "spring")
public interface UserCovertMapper extends BasicConvertObject<UserEntity, UserVo> {
    UserCovertMapper INSTANCE = Mappers.getMapper(UserCovertMapper.class);

    @Mappings({
        @Mapping(target = "name1", source = "name")
    })
    @Override
    UserVo toConvertVO(UserEntity userEntity);

    @Mappings({
        @Mapping(target = "name", source = "name1")
    })
    @Override
    UserEntity fromConvertEntity(UserVo userVo);
}
