package com.geek.project.geekuser.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.geek.project.geekcommon.page.PageWrapper;
import com.geek.project.geekuser.covert.UserCovertMapper;
import com.geek.project.geekuser.entity.UserEntity;
import com.geek.project.geekuser.mapper.UserMapper;
import com.geek.project.geekuser.service.UserService;
import com.geek.project.geekuserapi.model.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserEntity> implements UserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public UserVo findUserByAccount(UserVo userVo) {
        QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", userVo.getName1());
        UserEntity entity = userMapper.selectOne(queryWrapper);
        return UserCovertMapper.INSTANCE.toConvertVO(entity);
    }

    @Override
    public List<UserVo> queryList(UserVo userVo) {
        QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", userVo.getName1());
        List<UserEntity> lists = userMapper.selectList(queryWrapper);
        return UserCovertMapper.INSTANCE.toConvertVOList(lists);
    }

    @Override
    public PageWrapper<UserVo> queryPageList(Map<String, Object> params) {

//        IPage<User> page = this.page(
//                new Query<User>().getPage(params),
//                new QueryWrapper<User>()
//        );
        return null;
    }

    @Override
    public PageWrapper<UserVo> queryPageList(UserVo userVo, Long page, Long size) {
        QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("name", userVo.getName1());
        Page pageList = userMapper.selectPage(new Page<>(page, size), queryWrapper);
        return UserCovertMapper.INSTANCE.toConvertVOPage(pageList);
    }
}
